from threading import Timer

import sublime
import sublime_plugin

import subprocess


COMMAND_HISTORY_SIZE = 20


class CommandHistory:
    __instance = None

    @staticmethod
    def get_instance():
        if CommandHistory.__instance is None:
            return CommandHistory()
        else:
            return CommandHistory.__instance

    def __init__(self):
        self.history = []
        CommandHistory.__instance = self

    @property
    def last_command(self):
        return self.history[-1] if self.history else ''

    def update_history(self, val: str):
        if not val:
            return
        if self.history:
            if self.history[-1].startswith(val):
                # don't store part of previous command
                return
            if val.startswith(self.history[-1]):
                # don't store part of command
                self.history.pop()
        self.history.append(val)
        if len(self.history) > COMMAND_HISTORY_SIZE:
            self.history.pop(0)


def run_jq(text, query, line_endings,
           compact=False,
           slurp=False,
           sort_keys=False,
           raw_in=False,
           raw_out=False):
    options = []
    if compact:
        options.append("--compact-output")
    if slurp:
        options.append("--slurp")
    if sort_keys:
        options.append("--sort-keys")
    if raw_in:
        options.append("--raw-input")
    if raw_out:
        options.append("--raw-output")

    command = ["jq"] + options + [query]

    startup_info = None

    if sublime.platform().lower() == "windows":
        # Avoid showing console on windows (faster, and less flashy)
        startup_info = subprocess.STARTUPINFO()
        startup_info.dwFlags |= subprocess.STARTF_USESHOWWINDOW

    try:
        proc = subprocess.Popen(command,
                                stdin=subprocess.PIPE,
                                stdout=subprocess.PIPE,
                                stderr=subprocess.PIPE,
                                startupinfo=startup_info)
    except FileNotFoundError as e:
        print(e)
        message = "Jq not found. Install it, and make it available in your $PATH."
        message += "\n\nJq can be found at https://stedolan.github.io/jq/"
        sublime.error_message(message)
        return text

    try:
        out, err = proc.communicate(input=bytes(text, "utf-8"), timeout=10)
    except subprocess.TimeoutExpired:
        proc.kill()
        sublime.error_message("jq process killed, because it took more than 10s")
        return text
    if proc.returncode > 0:
        output = err
        # we don't want faulty commands in history
        CommandHistory.get_instance().history.pop()
    elif proc.returncode == 0:
        output = out
    else:
        return text

    formatted_text = output.decode("utf-8")
    if line_endings.lower() == "unix":
        formatted_text = formatted_text.replace("\r\n", "\n")
    return formatted_text


class JqApplyQueryCommand(sublime_plugin.TextCommand):
    def run(self, edit, query, compact=False, slurp=False, sort_keys=False, raw_in=False, raw_out=False):
        size = self.view.size()
        region = sublime.Region(0, size)

        sublime_version = int(sublime.version())
        if 3000 <= sublime_version < 4000:
            self.view.set_syntax_file("Packages/JavaScript/JSON.sublime-syntax")
        if sublime_version >= 4000:
            self.view.assign_syntax("scope:source.json")

        text = self.view.substr(region)
        formatted_text = run_jq(text, query,
                                self.view.line_endings(),
                                compact=compact,
                                slurp=slurp,
                                sort_keys=sort_keys,
                                raw_in=raw_in,
                                raw_out=raw_out)
        self.view.replace(edit, region, formatted_text)

    def is_enabled(self):
        return True


class JqFormatJsonCommand(sublime_plugin.TextCommand):
    def run(self, edit):
        self.view.run_command("jq_apply_query", {"query": "."})

    def is_enabled(self):
        return True


class JqFormatJsonCompactCommand(sublime_plugin.TextCommand):
    def run(self, edit):
        self.view.run_command("jq_apply_query", {"query": ".", "compact": True})

    def is_enabled(self):
        return True


class JqSetTextCommand(sublime_plugin.TextCommand):
    def run(self, edit, text):
        self.view.set_read_only(False)
        size = self.view.size()
        region = sublime.Region(0, size)
        self.view.replace(edit, region, text)
        self.view.set_read_only(True)


class JqTransformJsonCommand(sublime_plugin.TextCommand):
    def run(self, edit, initial=None):
        self.command_history = CommandHistory.get_instance()
        if self.view.element() is not None:
            # element is None for normal views, we don't want to run jq transform
            # in any other view
            if initial:
                self.command_history.update_history(initial)
            return
        self.view.set_read_only(True)
        self.last_timer = Timer(0.0001, lambda: 0, [])
        self.last_timer.start()
        size = self.view.size()
        region = sublime.Region(0, size)
        self.original_text = self.view.substr(region)
        if initial:
            initial_text = '.' if initial == '<History is empty>' else initial
        else:
            initial_text = self.command_history.last_command

        self.view.window().show_input_panel("jq query",
                                            initial_text,
                                            self.on_done,
                                            self.on_change,
                                            self.on_cancel)

    def on_done(self, query):
        self.view.set_read_only(False)
        return

    def on_change(self, query: str):
        # Run jq in the background, but only when the user stops typing for 250 ms
        # Aka, cancel the previous query in all cases
        self.last_timer.cancel()
        self.last_timer = Timer(0.25, self.trigger_jq, [query])
        self.last_timer.start()

    def trigger_jq(self, query):
        self.command_history.update_history(query)
        self.view.run_command("jq_set_text", {
            "text": run_jq(self.original_text, query, self.view.line_endings())
        })

    def on_cancel(self):
        # Reset text to the original one
        self.view.run_command("jq_set_text", {"text": self.original_text})
        self.view.set_read_only(False)


class HistoryInputHandler(sublime_plugin.ListInputHandler):
    def list_items(self):
        command_history = CommandHistory.get_instance()
        history = command_history.history or ['<History is empty>']
        return reversed(history)

    def name(self):
        return 'command'


class JqCommandHistoryCommand(sublime_plugin.TextCommand):
    def run(self, edit, command: str):
        self.view.run_command("jq_transform_json", {"initial": command})

    def input(self, args):
        return HistoryInputHandler()
